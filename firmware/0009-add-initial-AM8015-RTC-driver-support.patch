From 8febfe25c46a6a756c5c8595a5586a373342cfa4 Mon Sep 17 00:00:00 2001
From: Lukas Sigrist <lukas.sigrist@tik.ee.ethz.ch>
Date: Fri, 6 Jul 2018 16:32:25 +0200
Subject: [PATCH 09/33] add initial AM8015 RTC driver support

---
 arch/dev/am0815/README.md |   4 +
 arch/dev/am0815/am0815.c  | 424 ++++++++++++++++++++++++++++++++++++++++++++++
 arch/dev/am0815/am0815.h  | 141 +++++++++++++++
 3 files changed, 569 insertions(+)
 create mode 100644 arch/dev/am0815/README.md
 create mode 100644 arch/dev/am0815/am0815.c
 create mode 100644 arch/dev/am0815/am0815.h

diff --git a/arch/dev/am0815/README.md b/arch/dev/am0815/README.md
new file mode 100644
index 0000000..46db580
--- /dev/null
+++ b/arch/dev/am0815/README.md
@@ -0,0 +1,4 @@
+# External AM0815 RTC Driver
+
+This is a generic driver for Ambiq Micro AM08x5 real-time clock family. It has been tested with the following parts:
+- AB0815 (SPI)
diff --git a/arch/dev/am0815/am0815.c b/arch/dev/am0815/am0815.c
new file mode 100644
index 0000000..3bf9445
--- /dev/null
+++ b/arch/dev/am0815/am0815.c
@@ -0,0 +1,424 @@
+/**
+ * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
+ * All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice, this
+ *   list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of the copyright holder nor the names of its
+ *   contributors may be used to endorse or promote products derived from
+ *   this software without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE
+ * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
+ * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
+ * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ */
+/*---------------------------------------------------------------------------*/
+#include "contiki.h"
+#include "dev/spi.h"
+#include "gpio-hal.h"
+#include "sys/log.h"
+#include "am0815.h"
+
+#include <stdbool.h>
+#include <stdint.h>
+#include <stdio.h>
+
+/*---------------------------------------------------------------------------*/
+/* Log configuration */
+#define LOG_MODULE                    "am0815"
+#define LOG_LEVEL                     LOG_LEVEL_NONE
+/*---------------------------------------------------------------------------*/
+/* Helper function macros */
+#define BCD_TO_DEC(x)  (((x) & 0x0F) + 10 * ((x) >> 4))
+#define DEC_TO_BCD(x)  ((((x) / 10) << 4) | ((x) % 10))
+/*---------------------------------------------------------------------------*/
+#ifndef AM0815_SPI_CONTROLLER
+
+#define AM0815_SPI_CONTROLLER         0xFF /* No controller */
+
+#define AM0815_SPI_PIN_SCK            GPIO_HAL_PIN_UNKNOWN
+#define AM0815_SPI_PIN_MOSI           GPIO_HAL_PIN_UNKNOWN
+#define AM0815_SPI_PIN_MISO           GPIO_HAL_PIN_UNKNOWN
+#define AM0815_SPI_PIN_CS             GPIO_HAL_PIN_UNKNOWN
+
+#endif /* AM0815_SPI_CONTROLLER */
+/*---------------------------------------------------------------------------*/
+/* RTC instruction codes */
+#define AM0815_CODE_READ              0x00 /**< Read Register Command Flag */
+#define AM0815_CODE_WRITE             0x80 /**< Write Register Command Flag */
+/*---------------------------------------------------------------------------*/
+/* RTC registers */
+#define AM0815_HUNDREDTHS_ADDR        0x00 /**< hundredths address */
+#define AM0815_SECONDS_ADDR           0x01 /**< seconds address */
+#define AM0815_MINUTES_ADDR           0x02 /**< minutes address */
+#define AM0815_HOURS_ADDR             0x03 /**< hours address */
+#define AM0815_DATE_ADDR              0x04 /**< date address */
+#define AM0815_MONTHS_ADDR            0x05 /**< months address */
+#define AM0815_YEARS_ADDR             0x06 /**< years address */
+#define AM0815_WEEKDAYS_ADDR          0x07 /**< weekdays address */
+
+#define AM0815_HUNDREDTHS_ALARM_ADDR  0x08 /**< hundredths alarm address */
+#define AM0815_SECONDS_ALARM_ADDR     0x09 /**< seconds alarm address */
+#define AM0815_MINUTES_ALARM_ADDR     0x0A /**< minutes alarm address */
+#define AM0815_HOURS_ALARM_ADDR       0x0B /**< hours alarm address */
+#define AM0815_DATE_ALARM_ADDR        0x0C /**< date alarm address */
+#define AM0815_MONTHS_ALARM_ADDR      0x0D /**< months alarm address */
+#define AM0815_WEEKDAYS_ALARM_ADDR    0x0E /**< weekdays alarm address */
+
+#define AM0815_STATUS_ADDR            0x0F /**< status register address */
+#define AM0815_CONTROL1_ADDR          0x10 /**< control register 1 address */
+#define AM0815_CONTROL2_ADDR          0x11 /**< control register 2 address */
+
+#define AM0815_INTMASK_ADDR           0x12 /**< interrupt mask address */
+#define AM0815_SWQ_ADDR               0x13 /**< register address */
+#define AM0815_CAL_XT_ADDR            0x14 /**< register address */
+#define AM0815_CAL_RC_HIGH_ADDR       0x15 /**< register address */
+#define AM0815_CAL_RC_LOW_ADDR        0x16 /**< register address */
+#define AM0815_INT_POL_ADDR           0x17 /**< interrupt polarity address */
+
+#define AM0815_TIMER_CONTROL_ADDR     0x18 /**< register address */
+#define AM0815_TIMER_ADDR             0x19 /**< register address */
+#define AM0815_TIMER_INITIAL_ADDR     0x1A /**< register address */
+#define AM0815_WDT_ADDR               0x1B /**< register address */
+
+#define AM0815_OSC_CONTROL_ADDR       0x1C /**< oscillator control address */
+#define AM0815_OSC_STATUS_ADDR        0x1D /**< oscillator status address */
+
+#define AM0815_CONFIG_KEY_ADDR        0x1F /**< configuration key address */
+#define AM0815_TRICKLE_ADDR           0x20 /**< register address */
+
+#define AM0815_BREF_CONTROL_ADDR      0x21 /**< VBAT reference voltage address */
+#define AM0815_AFCTRL_ADDR            0x26 /**< autocalibration filter address */
+#define AM0815_BATMODE_ADDR           0x27 /**< battery mode IO config address */
+#define AM0815_ID_ADDR                0x28 /**< ID address */
+#define AM0815_ASTAT_ADDR             0x2F /**< analog status register address */
+#define AM0815_OCTRL_ADDR             0x30 /**< output control register address */
+
+#define AM0815_EXT_ADDR               0x3F /**< extension RAM address */
+#define AM0815_SRAM_ADDR              0x40 /**< standard data address */
+/*---------------------------------------------------------------------------*/
+#define AM0815_KEY_OSC                0xA1 /**< oscillator control register key */
+#define AM0815_KEY_RESET              0x3C /**< software reset key */
+#define AM0815_KEY_CONFIG             0x9D /**< protected configuration registers key */
+/*---------------------------------------------------------------------------*/
+#define AM0815_TIME_SIZE              7
+#define AM0815_RAM_SIZE               32
+/*---------------------------------------------------------------------------*/
+// #define VERIFY_PART_LOCKED           -2
+// #define VERIFY_PART_ERROR            -1
+// #define VERIFY_PART_POWERED_DOWN      0
+// #define VERIFY_PART_OK                1
+/*---------------------------------------------------------------------------*/
+static spi_device_t am0815_spi_configuration_default = {
+  .spi_controller = AM0815_SPI_CONTROLLER,
+  .pin_spi_sck = AM0815_SPI_PIN_SCK,
+  .pin_spi_miso = AM0815_SPI_PIN_MISO,
+  .pin_spi_mosi = AM0815_SPI_PIN_MOSI,
+  .pin_spi_cs = AM0815_SPI_PIN_CS,
+  .spi_bit_rate = 2000000,
+  .spi_pha = 0,
+  .spi_pol = 0,
+};
+/*---------------------------------------------------------------------------*/
+/**
+ * Convert register values to time structure
+ */
+static void
+am0815_reg_to_time(const uint8_t *reg, am0815_tm_t *timeptr)
+{
+  timeptr->split = BCD_TO_DEC(reg[0]);
+  timeptr->sec = BCD_TO_DEC(reg[1] & 0x7F);
+  timeptr->min = BCD_TO_DEC(reg[2] & 0x7F);
+  timeptr->hour = BCD_TO_DEC(reg[3] & 0x3F);
+  timeptr->day = BCD_TO_DEC(reg[4] & 0x3F);
+  timeptr->month = BCD_TO_DEC(reg[5] & 0x1F);
+  timeptr->year = BCD_TO_DEC(reg[6]);
+}
+/*---------------------------------------------------------------------------*/
+/**
+ * Convert time structure to register values
+ */
+static void
+am0815_time_to_reg(const am0815_tm_t *timeptr, uint8_t *reg)
+{
+  reg[0] = DEC_TO_BCD(timeptr->split);
+  reg[1] = DEC_TO_BCD(timeptr->sec);
+  reg[2] = DEC_TO_BCD(timeptr->min);
+  reg[3] = DEC_TO_BCD(timeptr->hour);
+  reg[4] = DEC_TO_BCD(timeptr->day);
+  reg[5] = DEC_TO_BCD(timeptr->month);
+  reg[6] = DEC_TO_BCD(timeptr->year);
+}
+/*---------------------------------------------------------------------------*/
+/**
+ * Get SPI configuration, return default configuration if NULL
+ */
+static spi_device_t*
+get_spi_conf(spi_device_t* conf)
+{
+  if(conf == NULL) {
+    return &am0815_spi_configuration_default;
+  }
+  return conf;
+}
+/*---------------------------------------------------------------------------*/
+/**
+ * Pull CS line low to start communication
+ */
+static bool
+select_on_bus(spi_device_t* spi_config)
+{
+  if(spi_select(spi_config) == SPI_DEV_STATUS_OK) {
+    LOG_DBG("SPI selected\n");
+    return true;
+  }
+  LOG_DBG("SPI select FAILED\n");
+  return false;
+}
+/*---------------------------------------------------------------------------*/
+/**
+ * Pull CS high to stop communication
+ */
+static void
+deselect(spi_device_t* spi_config)
+{
+  spi_deselect(spi_config);
+  LOG_DBG("SPI deselected\n");
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_open(spi_device_t *conf)
+{
+  spi_device_t *spi_config = get_spi_conf(conf);
+
+  /* Check if platform has ext-fram */
+  if(spi_config->pin_spi_sck == GPIO_HAL_PIN_UNKNOWN) {
+    LOG_WARN("AM0815 RTC not present in platform!\n");
+    return false;
+  }
+
+  if(spi_acquire(spi_config) != SPI_DEV_STATUS_OK) {
+    LOG_DBG("SPI acquire FAILED\n");
+    return false;
+  }
+
+  deselect(spi_config);
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_close(spi_device_t *conf)
+{
+  spi_device_t *spi_config = get_spi_conf(conf);
+
+  if(spi_release(spi_config) != SPI_DEV_STATUS_OK) {
+    return false;
+  }
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_read_reg(spi_device_t *conf, uint8_t addr, uint8_t length, uint8_t *buf)
+{
+  bool ret;
+  uint8_t cmd;
+
+  spi_device_t *spi_config = get_spi_conf(conf);
+
+  // assemble read command
+  cmd = AM0815_CODE_READ | (addr & 0x7F);
+
+  if(select_on_bus(spi_config) == false) {
+    return false;
+  }
+
+  LOG_DBG("read register: address\n");
+  if(spi_write(spi_config, &cmd, sizeof(cmd)) != SPI_DEV_STATUS_OK) {
+    /* failure */
+    deselect(spi_config);
+    return false;
+  }
+
+  LOG_DBG("read register: data\n");
+  ret = (spi_read(spi_config, buf, length) == SPI_DEV_STATUS_OK);
+
+  deselect(spi_config);
+
+  return ret;
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_write_reg(spi_device_t *conf, uint8_t addr, uint8_t length, const uint8_t *buf)
+{
+  bool ret;
+  uint8_t cmd;
+
+  spi_device_t *spi_config = get_spi_conf(conf);
+
+  // assemble SRAM write command
+  cmd = AM0815_CODE_WRITE | (addr & 0x7F);
+
+  if(select_on_bus(spi_config) == false) {
+    return false;
+  }
+
+  LOG_DBG("write register: command\n");
+  if(spi_write(spi_config, &cmd, sizeof(cmd)) != SPI_DEV_STATUS_OK) {
+    /* failure */
+    deselect(spi_config);
+    return false;
+  }
+
+  LOG_DBG("write register: data\n");
+  ret = (spi_write(spi_config, buf, length) == SPI_DEV_STATUS_OK);
+
+  deselect(spi_config);
+
+  return ret;
+}
+/*---------------------------------------------------------------------------*/
+inline bool
+am0815_set_reg(spi_device_t *conf, uint8_t addr, uint8_t value) {
+  return am0815_write_reg(conf, addr, 1, &value);
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_set_mode(spi_device_t *conf, am0815_mode_t mode)
+{
+  bool ret;
+  if (mode == AM0815_MODE_STOP) {
+    // halt the RTC
+    return am0815_set_reg(conf, AM0815_CONTROL1_ADDR, 0x91);
+  }
+  
+  // get mode specific osc register value
+  uint8_t reg;
+  switch (mode) {
+  default:
+    LOG_DBG("AM0815 invalid mode, fallback to default\n");
+  case AM0815_MODE_CRYSTAL:
+    reg = 0x00;
+    break;
+  case AM0815_MODE_RC_ONLY:
+    reg = 0x80;
+    break;
+  case AM0815_MODE_RC_BACKUP:
+    reg = 0x90;
+    break;
+  case AM0815_MODE_AUTOCALIBRATE_9MIN:
+    reg = 0xC0;
+    break;
+  case AM0815_MODE_AUTOCALIBRATE_17MIN:
+    reg = 0xE0;
+    break;
+  }
+  // set oscillator register
+  // 1. unlock register
+  ret = am0815_set_reg(conf, AM0815_CONFIG_KEY_ADDR, AM0815_KEY_OSC);
+  if (ret == false) {
+    return false;
+  }
+  // 2. set register value
+  ret = am0815_set_reg(conf, AM0815_OSC_CONTROL_ADDR, reg);
+  if (ret == false) {
+    return false;
+  }
+  // 3. unlock register (ignore errors)
+  am0815_set_reg(conf, AM0815_CONFIG_KEY_ADDR, 0x00);
+
+  // resume RTC in case it was disabled
+  ret = am0815_set_reg(conf, AM0815_CONTROL1_ADDR, 0x11);
+
+  return ret;
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_get_time(spi_device_t *conf, am0815_tm_t *timeptr)
+{
+  uint8_t rbuf[AM0815_TIME_SIZE];
+  bool ret;
+
+  // read data
+  ret = am0815_read_reg(conf, AM0815_HUNDREDTHS_ADDR, AM0815_TIME_SIZE, rbuf);
+  if (ret == false) {
+    return false;
+  }
+
+  // perform time transformation
+  am0815_reg_to_time(rbuf, timeptr);
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_set_time(spi_device_t *conf, const am0815_tm_t *timeptr)
+{
+  uint8_t wbuf[AM0815_TIME_SIZE];
+  bool ret;
+
+  // perform time to register values transformation
+  am0815_time_to_reg(timeptr, wbuf);
+
+  // write data
+  ret = am0815_write_reg(conf, AM0815_HUNDREDTHS_ADDR, AM0815_TIME_SIZE, wbuf);
+
+  return ret;
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_read_sram(spi_device_t *conf, uint8_t offset, uint8_t length, uint8_t *buf)
+{
+  // calculate SRAM address
+  uint8_t addr = AM0815_CODE_READ | (AM0815_SRAM_ADDR + (offset & 0x3F));
+
+  // read data
+  return am0815_read_reg(conf, addr, length, buf);
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_write_sram(spi_device_t *conf, uint8_t offset, uint8_t length, const uint8_t *buf)
+{
+  // calculate SRAM address
+  uint8_t addr = AM0815_CODE_READ | (AM0815_SRAM_ADDR + (offset & 0x3F));
+
+  // read data
+  return am0815_write_reg(conf, addr, length, buf);
+}
+/*---------------------------------------------------------------------------*/
+bool
+am0815_init(spi_device_t *conf)
+{
+  if(am0815_open(conf) == false) {
+    return false;
+  }
+
+  // set configuration registers
+  am0815_set_reg(conf, AM0815_STATUS_ADDR, 0x80);
+
+  if(am0815_close(conf) == false) {
+    return false;
+  }
+
+  LOG_INFO("AM0815 init successful\n");
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
diff --git a/arch/dev/am0815/am0815.h b/arch/dev/am0815/am0815.h
new file mode 100644
index 0000000..1a68858
--- /dev/null
+++ b/arch/dev/am0815/am0815.h
@@ -0,0 +1,141 @@
+/**
+ * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
+ * All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice, this
+ *   list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of the copyright holder nor the names of its
+ *   contributors may be used to endorse or promote products derived from
+ *   this software without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE
+ * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
+ * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
+ * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ */
+
+/*---------------------------------------------------------------------------*/
+#ifndef AM0815_H_
+#define AM0815_H_
+/*---------------------------------------------------------------------------*/
+#include "dev/spi.h"
+#include <stdint.h>
+#include <stdlib.h>
+#include <stdbool.h>
+/*---------------------------------------------------------------------------*/
+/**
+ * AM0815 operation mode definitions
+ */
+typedef enum {
+  AM0815_MODE_STOP, /**< stop the RTC clocks */
+  AM0815_MODE_CRYSTAL, /**< crystal oscillator (default mode after reset) */
+  AM0815_MODE_AUTOCALIBRATE_9MIN, /**< RC oscillator with XT auto calibration every 512 sec */
+  AM0815_MODE_AUTOCALIBRATE_17MIN, /**< RC oscillator with XT auto calibration every 1024 sec */
+  AM0815_MODE_RC_ONLY,     /**< RC oscillator only */
+  AM0815_MODE_RC_BACKUP,   /**< crystal oscillator with fallback to RC oscillator when on VBAT supply */
+} am0815_mode_t;
+
+/**
+ * Date time structure to work with the RTC
+ */
+typedef struct am0815_tm
+{
+  uint8_t split;   /**< hunderedths split seconds after seconds (0-99) */
+  uint8_t sec;     /**< seconds after minutes (0-59)*/
+  uint8_t min;     /**< minutes after hours (0-59) */
+  uint8_t hour;    /**< hours after midnight (0-23) */
+  uint8_t day;     /**< day of the month (1-31)*/
+  uint8_t month;   /**< month of the year (1-12) */
+  uint8_t year;    /**< year after 2000 (0-99) */
+} am0815_tm_t;
+/*---------------------------------------------------------------------------*/
+
+/**
+ * \brief Initialize AM0815 RTC driver.
+ * \param conf SPI bus configuration struct. NULL for default.
+ * \return True when successful.
+ */
+bool am0815_open(spi_device_t *conf);
+
+/**
+ * \brief Close the AM0815 RTC driver
+ * \param conf SPI bus configuration struct. NULL for default.
+ * \return True when successful.
+ *
+ * This call will put the device in its lower power mode (power down).
+ */
+bool am0815_close(spi_device_t *conf);
+
+/**
+ * \brief Set operation mode of the AM0815 RTC
+ * \param conf SPI bus configuration struct. NULL for default.
+ * \param mode The operation mode to configure.
+ * \return True when successful.
+ */
+bool am0815_set_mode(spi_device_t *conf, am0815_mode_t mode);
+
+/**
+ * \brief Get the current data/time value of AM0815 RTC
+ * \param conf SPI bus configuration struct. NULL for default.
+ * \param timeptr Pointer to date/time to to write the current RTC value to.
+ * \return True when successful.
+ * 
+ * The output date/time structure has to be allocated by the caller.
+ */
+bool am0815_get_time(spi_device_t *conf, am0815_tm_t *timeptr);
+
+/**
+ * \brief Set the time of AM0815 RTC
+ * \param conf SPI bus configuration struct. NULL for default.
+ * \param timeptr Pointer to date/time to set the RTC value to.
+ * \return True when successful.
+ */
+bool am0815_set_time(spi_device_t *conf, const am0815_tm_t *timeptr);
+
+/**
+ * \brief Read the integrated SRAM.
+ * \param conf SPI bus configuration struct. NULL for default.
+ * \param offset Address to read from
+ * \param length Number of bytes to read
+ * \param buf Buffer where to store the read bytes
+ * \return True when successful.
+ *
+ * buf must be allocated by the caller
+ */
+bool am0815_read_sram(spi_device_t *conf, uint8_t offset, uint8_t length, uint8_t *buf);
+
+/**
+ * \brief Write to the integrated SRAM.
+ * \param conf SPI bus configuration struct. NULL for default.
+ * \param offset Address to write to
+ * \param length Number of bytes to write
+ * \param buf Buffer holding the bytes to be written
+ *
+ * \return True when successful.
+ */
+bool am0815_write_sram(spi_device_t *conf, uint8_t offset, uint8_t length, const uint8_t *buf);
+
+/**
+ * \brief Initialise the AM0815 RTC
+ * \param conf SPI bus configuration struct. NULL for default.
+ *
+ */
+bool am0815_init(spi_device_t *conf);
+/*---------------------------------------------------------------------------*/
+#endif /* AM0815_H_ */
+/*---------------------------------------------------------------------------*/
-- 
2.7.4

