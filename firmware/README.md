# Transient BLE Sensor Node Firmware

The firmware of the sensor node is based on contiki-ng (<https://github.com/contiki-ng/contiki-ng>). Follow the repository setup procedure below to get the contiki sources and the patches that contain our sensor node firmware and documentation.

Once the repository is setup, you find the sensor node application code in `contiki-ng/examples/platform-specific/cc26xx/transient-ble` with detailed documentation in the accompanying [README.md](contiki-ng/examples/platform-specific/cc26xx/transient-ble/README.md) file.


## Repository Setup

To initialize the repository, clone the contiki-ng repository at version v4.1, update its submodules and apply our firmware code in the patches of this folder.

These steps are performed using the following commands:

```bash
# clone contiki-ng release v4.1
git clone -b release/v4.1 https://github.com/contiki-ng/contiki-ng.git
cd contiki-ng
git submodule update --init --recursive
# apply transient BLE sensor node patches on new transient-ble branch
git checkout -b transient-ble
git am ../*.patch
```


## Getting Started with the Transient Sensor Node

Head to the [README.md](contiki-ng/examples/platform-specific/cc26xx/transient-ble/README.md) in the newly setup repository to get started with the sensor node.
