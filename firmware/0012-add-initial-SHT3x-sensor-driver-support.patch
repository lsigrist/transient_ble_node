From b2bfdf23c07464d55c715a4649b49531d338c4f7 Mon Sep 17 00:00:00 2001
From: Lukas Sigrist <lukas.sigrist@tik.ee.ethz.ch>
Date: Mon, 9 Jul 2018 23:26:16 +0200
Subject: [PATCH 12/33] add initial SHT3x sensor driver support

---
 arch/dev/sht3x/sht3x-sensor.c |  74 +++++++++++++
 arch/dev/sht3x/sht3x-sensor.h |  40 +++++++
 arch/dev/sht3x/sht3x.c        | 240 ++++++++++++++++++++++++++++++++++++++++++
 arch/dev/sht3x/sht3x.h        | 111 +++++++++++++++++++
 4 files changed, 465 insertions(+)
 create mode 100644 arch/dev/sht3x/sht3x-sensor.c
 create mode 100644 arch/dev/sht3x/sht3x-sensor.h
 create mode 100644 arch/dev/sht3x/sht3x.c
 create mode 100644 arch/dev/sht3x/sht3x.h

diff --git a/arch/dev/sht3x/sht3x-sensor.c b/arch/dev/sht3x/sht3x-sensor.c
new file mode 100644
index 0000000..0a5e6e4
--- /dev/null
+++ b/arch/dev/sht3x/sht3x-sensor.c
@@ -0,0 +1,74 @@
+/*
+ * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
+ * All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of the copyright holder nor the names of its
+ *   contributors may be used to endorse or promote products derived from
+ *   this software without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE
+ * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
+ * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
+ * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ */
+/*---------------------------------------------------------------------------*/
+#include "contiki.h"
+#include "sht3x.h"
+#include "sht3x-sensor.h"
+
+#include <stdbool.h>
+#include <stdint.h>
+/*---------------------------------------------------------------------------*/
+#define SHT3X_STATE_SLEEP     			0
+#define SHT3X_STATE_ACTIVE       		1
+#define SHT3X_STATE_DATA_READY   		2
+#define SHT3X_TYPE_TEMPERATURE      0
+#define SHT3X_TYPE_HUMIDITY         1
+/*---------------------------------------------------------------------------*/
+static int sht3x_state = SHT3X_STATE_SLEEP;
+/*---------------------------------------------------------------------------*/
+static int value(int type)
+{
+  uint16_t temperature_raw, humidity_raw;
+
+  sht3x_read_values(NULL, SHT3X_REPEATABILITY_LOW,
+                    &temperature_raw, &humidity_raw);
+
+  if (type == SHT3X_TYPE_TEMPERATURE) {
+    return sht3x_convert_temperature(temperature_raw);
+  } else {
+    return sht3x_convert_humidity(humidity_raw);
+  }
+}
+/*---------------------------------------------------------------------------*/
+static int
+configure(int type, int enable)
+{
+  return sht3x_init(NULL);
+}
+/*---------------------------------------------------------------------------*/
+static int
+status(int type)
+{
+  return sht3x_state;
+}
+/*---------------------------------------------------------------------------*/
+SENSORS_SENSOR(sht3x_sensor, "SHT3x", value, configure, status);
+/*---------------------------------------------------------------------------*/
diff --git a/arch/dev/sht3x/sht3x-sensor.h b/arch/dev/sht3x/sht3x-sensor.h
new file mode 100644
index 0000000..b27413f
--- /dev/null
+++ b/arch/dev/sht3x/sht3x-sensor.h
@@ -0,0 +1,40 @@
+/*
+ * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
+ * All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of the copyright holder nor the names of its
+ *   contributors may be used to endorse or promote products derived from
+ *   this software without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE
+ * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
+ * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
+ * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ */
+/*---------------------------------------------------------------------------*/
+#ifndef SHT3X_SENSOR_H_
+#define SHT3X_SENSOR_H_
+/*---------------------------------------------------------------------------*/
+#include "lib/sensors.h"
+/*---------------------------------------------------------------------------*/
+extern const struct sensors_sensor sht3x_sensor;
+/*---------------------------------------------------------------------------*/
+#endif /* SHT3X_SENSOR_H_ */
+/*---------------------------------------------------------------------------*/
diff --git a/arch/dev/sht3x/sht3x.c b/arch/dev/sht3x/sht3x.c
new file mode 100644
index 0000000..589de1c
--- /dev/null
+++ b/arch/dev/sht3x/sht3x.c
@@ -0,0 +1,240 @@
+/*
+ * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
+ * All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of the copyright holder nor the names of its
+ *   contributors may be used to endorse or promote products derived from
+ *   this software without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE
+ * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
+ * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
+ * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ */
+/*---------------------------------------------------------------------------*/
+#include "contiki.h"
+#include "gpio-hal.h"
+#include "sys/log.h"
+#include "ti-lib.h"
+#include "board-i2c.h"
+#include "sht3x.h"
+
+#include <stdbool.h>
+#include <stdint.h>
+#include <stdio.h>
+/*---------------------------------------------------------------------------*/
+/* Log configuration */
+#define LOG_MODULE                    "sht-3x"
+#define LOG_LEVEL                     LOG_LEVEL_DBG
+/*---------------------------------------------------------------------------*/
+#ifndef SHT3X_I2C_CONTROLLER
+
+#define SHT3X_I2C_CONTROLLER          0xFF /* No controller */
+
+#define SHT3X_I2C_PIN_SCL             GPIO_HAL_PIN_UNKNOWN
+#define SHT3X_I2C_PIN_SDA             GPIO_HAL_PIN_UNKNOWN
+
+#define SHT3X_I2C_ADDRESS             0x44
+
+#endif /* SHT3X_I2C_CONTROLLER */
+/*---------------------------------------------------------------------------*/
+// single shot commands
+#if SHT3X_USE_CLOCKSTRETCH
+#define SHT3X_CODE_SINGLE_HIGH        0x2C06
+#define SHT3X_CODE_SINGLE_MEDIUM      0x2C0D
+#define SHT3X_CODE_SINGLE_LOW         0x2C10
+#else
+#define SHT3X_CODE_SINGLE_HIGH        0x2400
+#define SHT3X_CODE_SINGLE_MEDIUM      0x240B
+#define SHT3X_CODE_SINGLE_LOW         0x2416
+#endif
+/*---------------------------------------------------------------------------*/
+// periodic mode command MSB
+// #define SHT3X_PERIOD_0_5_MPS          0x20
+// #define SHT3X_PERIOD_1_MPS            0x21
+// #define SHT3X_PERIOD_2_MPS            0x22
+// #define SHT3X_PERIOD_4_MPS            0x23
+// #define SHT3X_PERIOD_10_MPS           0x27
+// #define SHT3X_CODE_FETCH              0xE000
+// #define SHT3X_CODE_ART                0x2B32
+/*---------------------------------------------------------------------------*/
+// general commands
+#define SHT3X_CODE_BREAK              0x3093
+#define SHT3X_CODE_SOFT_RESET         0x30A2
+#define SHT3X_CODE_STATUS             0xF32D
+#define SHT3X_CODE_CLEAR_STATUS       0x3041
+#define SHT3X_CODE_HEATER_ENABLE      0x306D
+#define SHT3X_CODE_HEATER_DISABLE     0x3066
+/*---------------------------------------------------------------------------*/
+int32_t
+sht3x_convert_temperature(uint16_t raw)
+{
+#if SHT3X_USE_FAHRENHEIT
+  return -4500 + (17500 * (int32_t)raw) / 65535;
+#else
+  return -4900 + (31500 * (int32_t)raw) / 65535;
+#endif
+}
+/*---------------------------------------------------------------------------*/
+int32_t
+sht3x_convert_humidity(uint16_t raw)
+{
+  return (10000 * (int32_t)raw) / 65535; 
+}
+/*---------------------------------------------------------------------------*/
+static void
+select_on_bus(void *conf)
+{
+  // select sensor slave
+  LOG_DBG("I2C select");
+  board_i2c_select(SHT3X_I2C_ADDRESS);
+}
+/*---------------------------------------------------------------------------*/
+bool
+sht3x_read(void *conf, uint8_t *rbuf, uint8_t length)
+{
+  bool ret;
+
+  // read the actual data
+  LOG_DBG("I2C read");
+  ret = board_i2c_read(rbuf, length);
+  if (!ret) {
+    LOG_DBG("I2C read FAILED");
+    return ret;
+  }
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
+bool
+sht3x_write(void *conf, uint16_t command)
+{
+  uint8_t wbuffer[2];
+  bool ret;
+  wbuffer[0] = (uint8_t)(command >> 8);
+  wbuffer[1] = (uint8_t)(command & 0xff);
+
+  // read the actual data
+  LOG_DBG("I2C write");
+  ret = board_i2c_write(wbuffer, sizeof(wbuffer));
+  if (!ret) {
+    LOG_DBG("I2C read FAILED");
+    return ret;
+  }
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
+bool
+sht3x_init(void *conf)
+{
+  // nothing needed to initialize, test communication by issuing soft reset
+  return sht3x_soft_reset(conf);
+}
+/*---------------------------------------------------------------------------*/
+bool
+sht3x_read_values(void *conf, sht3x_repeatability_t repeatability,
+                  uint16_t *temperature, uint16_t *humidity)
+{
+  uint8_t rbuf[6];
+  uint16_t cmd;
+  bool ret;
+
+  // get command for chosen repeatability level
+  switch(repeatability) {
+  case SHT3X_REPEATABILITY_HIGH:
+    cmd = SHT3X_CODE_SINGLE_HIGH;
+    break;
+  case SHT3X_REPEATABILITY_MEDIUM:
+    cmd = SHT3X_CODE_SINGLE_MEDIUM;
+    break;
+  case SHT3X_REPEATABILITY_LOW:
+  default:
+    cmd = SHT3X_CODE_SINGLE_LOW;
+    break;
+  }
+
+  // select device
+  select_on_bus(conf);
+
+  // request status
+  ret = sht3x_write(conf, cmd);
+  if (!ret) {
+    return ret;
+  }
+
+  // read register data
+  ret = sht3x_read(conf, rbuf, sizeof(rbuf));
+  if (!ret) {
+    return ret;
+  }
+
+  // assemble temperature and humidity values
+  *temperature = ((uint16_t)rbuf[0] << 8) | ((uint16_t)rbuf[1]);
+  *humidity = ((uint16_t)rbuf[3] << 8) | ((uint16_t)rbuf[4]);
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
+bool
+sht3x_read_status(void *conf, uint16_t *status)
+{
+  uint8_t rbuf[2];
+  bool ret;
+
+  // select device
+  select_on_bus(conf);
+
+  // request status
+  ret = sht3x_write(conf, SHT3X_CODE_STATUS);
+  if (!ret) {
+    return ret;
+  }
+
+  // read register data
+  ret = sht3x_read(conf, rbuf, sizeof(rbuf));
+  if (!ret) {
+    return ret;
+  }
+
+  // assemble status register value
+  *status = ((uint16_t)rbuf[0] << 8) | ((uint16_t)rbuf[1]);
+
+  return true;
+}
+/*---------------------------------------------------------------------------*/
+bool
+sht3x_clear_status(void *conf)
+{
+  // select device
+  select_on_bus(conf);
+
+  return sht3x_write(conf, SHT3X_CODE_CLEAR_STATUS);
+}
+/*---------------------------------------------------------------------------*/
+bool
+sht3x_soft_reset(void *conf)
+{
+  // select device
+  select_on_bus(conf);
+
+  return sht3x_write(conf, SHT3X_CODE_SOFT_RESET); 
+}
+/*---------------------------------------------------------------------------*/
diff --git a/arch/dev/sht3x/sht3x.h b/arch/dev/sht3x/sht3x.h
new file mode 100644
index 0000000..5f06f3b
--- /dev/null
+++ b/arch/dev/sht3x/sht3x.h
@@ -0,0 +1,111 @@
+/*
+ * Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
+ * All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of the copyright holder nor the names of its
+ *   contributors may be used to endorse or promote products derived from
+ *   this software without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE
+ * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
+ * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
+ * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+ */
+/*---------------------------------------------------------------------------*/
+#ifndef SHT3X_H_
+#define SHT3X_H_
+/*---------------------------------------------------------------------------*/
+#include "dev/spi.h"
+#include <stdbool.h>
+#include <stdint.h>
+#include <stdlib.h>
+/*---------------------------------------------------------------------------*/
+#define SHT3X_USE_CLOCKSTRETCH  0
+#define SHT3X_USE_FAHRENHEIT    0
+/*---------------------------------------------------------------------------*/
+/**
+ * SHT3x measurement repeatibility
+ */
+typedef enum {
+  SHT3X_REPEATABILITY_LOW,    /**< low measurement repeatablity */
+  SHT3X_REPEATABILITY_MEDIUM, /**< medium measurement repeatablity */
+  SHT3X_REPEATABILITY_HIGH,   /**< hihg measurement repeatablity */
+} sht3x_repeatability_t;
+/*---------------------------------------------------------------------------*/
+
+/**
+ * Convert the raw reading into absolute temperature.
+ * \param raw The raw temperature measurements reading
+ * \return The absolute temperature in 100's of degrees Celsius/Fahrenheit
+ * 
+ * Units used are configured using the SHT3X_USE_FAHRENHEIT header define.
+ */
+int32_t sht3x_convert_temperature(uint16_t raw);
+
+/**
+ * Convert the raw reading into relative humidity.
+ * \param raw The raw humidity measurements reading
+ * \return The relative humidity in 100's of percents
+ */
+int32_t sht3x_convert_humidity(uint16_t raw);
+
+/**
+ * \brief Initialise the SHT3x sensor
+ * \param conf Unused pointer for future I2C device specification
+ * \return Wheter the operation succeeded
+ */
+bool sht3x_init(void *conf);
+
+/**
+ * \brief Read the sensor values of the SHT3x sensor.
+ * \param conf Unused pointer for future I2C device specification
+ * \param repeatibility Sensing repeatability level to use for measuring
+ * \param temperature The temperatature value output buffer
+ * \param humidity The humidity value output buffer
+ * \return Wheter the operation succeeded
+ */
+bool sht3x_read_values(void *conf, sht3x_repeatability_t repeatability,
+                       uint16_t *temperature, uint16_t *humidity);
+
+/**
+ * \brief Get the status of the SHT3x sensor.
+ * \param conf Unused pointer for future I2C device specification
+ * \param status The status register output buffer
+ * \return Wheter the operation succeeded
+ */
+bool sht3x_read_status(void *conf, uint16_t *status);
+
+/**
+ * \brief Clear the status of the SHT3x sensor.
+ * \param conf Unused pointer for future I2C device specification
+ * \return Wheter the operation succeeded
+ */
+bool sht3x_clear_status(void *conf);
+
+/**
+ * \brief Soft reset the SHT3x sensor.
+ * \param conf Unused pointer for future I2C device specification
+ * \return Wheter the operation succeeded
+ */
+bool sht3x_soft_reset(void *conf);
+
+/*---------------------------------------------------------------------------*/
+#endif /* SHT3X_H_ */
+/*---------------------------------------------------------------------------*/
-- 
2.7.4

